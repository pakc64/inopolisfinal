package ru.pakc.servece;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.pakc.model.Merch;
import ru.pakc.model.User;
import ru.pakc.repository.MerchRepo;
import ru.pakc.repository.UserRepo;

import java.security.Principal;
import java.util.List;


@Service
@RequiredArgsConstructor
@Slf4j
public class MerchService {

    private final MerchRepo merchRepo;
    private final UserRepo userRepo;

    public List findAll() {
        return merchRepo.findAll();
    }

    public void saveMerch(Merch merch, Principal principal) {
        merch.setUser(getUserByPrincipal(principal));
        merchRepo.save(merch);
    }

    public void deleteMerch(Long id) {
        merchRepo.deleteById(id);
    }

    public Merch getMerchById(Long id) {
        return merchRepo.findById(id).orElse(null);
    }


    public User getUserByPrincipal(Principal principal) {
        if (principal == null) return new User();
        return userRepo.findByEmail(principal.getName());
    }

    public void updateMerch(Merch newMerch) {
        Merch merchToUpdate = getMerchById(newMerch.getId());
        merchToUpdate.setName(newMerch.getName());
        merchToUpdate.setPrice(newMerch.getPrice());
        merchToUpdate.setDescription(newMerch.getDescription());
        merchRepo.save(merchToUpdate);
    }
}
