package ru.pakc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pakc.model.User;

public interface UserRepo extends JpaRepository<User, Long> {

    User findByEmail(String email);
}
