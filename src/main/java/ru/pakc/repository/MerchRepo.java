package ru.pakc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pakc.model.Merch;

import java.util.List;

@Repository
public interface MerchRepo extends JpaRepository<Merch, Long> {
    List<Merch> findByName(String name);

}
