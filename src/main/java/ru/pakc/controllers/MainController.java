package ru.pakc.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.pakc.model.User;
import ru.pakc.servece.MerchService;

import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class MainController {
    private final MerchService merchService;

    @GetMapping("/")
    public String mainPage() {
        return "main";
    }

    @GetMapping("/merchView")
    public String merchView(@AuthenticationPrincipal User user, Model model, Principal principal) {
        if(user.getRoles().contains("ROLE_ADMIN")) {
            model.addAttribute("isAdmin", true);
        } else {
            model.addAttribute("isAdmin", false);
        }
        model.addAttribute("merch", merchService.findAll());
        model.addAttribute("user", merchService.getUserByPrincipal(principal));
        return "merch-view";
    }
}
