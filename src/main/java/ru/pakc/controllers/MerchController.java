package ru.pakc.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pakc.model.Merch;
import ru.pakc.model.User;
import ru.pakc.servece.MerchService;
import ru.pakc.servece.UserService;

import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class MerchController {
    private final MerchService merchService;

    @GetMapping("/merch")
    public String homePage(@AuthenticationPrincipal User user, Model model, Principal principal) {
        if(user.getRoles().contains("ROLE_ADMIN")) {
            model.addAttribute("isAdmin", true);
        } else {
            model.addAttribute("isAdmin", false);
        }
        model.addAttribute("merch", merchService.findAll());
        model.addAttribute("user", merchService.getUserByPrincipal(principal));
        return "merch";
    }

    @GetMapping("/merch/{id}")
    public String merchInfo(@PathVariable Long id, Model model) {
        model.addAttribute("merch", merchService.getMerchById(id));
        return "merch-info";
    }

    @PostMapping("/merch/create")
    public String createMerch(Merch merch, Principal principal) {
        merchService.saveMerch(merch, principal);
        return "redirect:/admin";
    }

    @GetMapping("/merchApdDel/{id}")
    public String merchUpdateDelete(@PathVariable Long id, Model model) {
        model.addAttribute("merch", merchService.getMerchById(id));
        return "merch-update-delete";
    }

    @PostMapping("/merch/delete/{id}")
    public String deleteMerch(@PathVariable Long id) {
        merchService.deleteMerch(id);
        return "redirect:/admin";

    }

    @PostMapping("merch/update/{id}")
    public String updateMerch(Merch merch) {
        merchService.updateMerch(merch);
        return "redirect:/merchApdDel/{id}";
    }

}
